FROM php:7.4-fpm



# Arguments defined in docker-compose.yml

ARG user

ARG uid

USER root
RUN usermod -aG docker jenkins



# Install system dependencies

RUN apt-get update && apt-get install -y \
    git \
    curl \
    libpng-dev \
    libonig-dev \
    libxml2-dev \
    zip \
    unzip \
    libpq-dev \
    npm



# Clear cache

RUN apt-get clean && rm -rf /var/lib/apt/lists/*



# Install PHP extensions

RUN docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath gd pdo pdo_pgsql


#install redis

RUN pecl install redis \
    && docker-php-ext-enable redis


# Get latest Composer

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer



# Create system user to run Composer and Artisan Commands

RUN useradd -G www-data,root -u 1000 -d /home/sammy sammy

RUN mkdir -p /home/sammy/.composer && \

    chown -R sammy:sammy /home/sammy



# Set working directory

WORKDIR /var/www


USER sammy
